vim9script

# commentary.vim - Comment stuff out
# Maintainer:   vim plugin ports <https://gitlab.com/vim-plugin-ports>
# Version:      0.1

if exists("g:loaded_commentary") || v:version < 703
  finish
endif
g:loaded_commentary = 1

def Surroundings()
  return split(get(b:, 'commentary_format', substitute(substitute(substitute(
        \ &commentstring, '^$', '%s', ''), '\S\zs%s',' %s', '') ,'%s\ze\S', '%s ', '')), '%s', 1)
enddef

def Strip_white_space(l,r,line)
  var [l, r] = [a:l, a:r]
  if l[-1:] ==# ' ' && stridx(a:line,l) == -1 && stridx(a:line,l[0:-2]) == 0
    var l = l[:-2]
  endif
  if r[0] ==# ' ' && (' ' . a:line)[-strlen(r)-1:] != r && a:line[-strlen(r):] == r[1:]
    var r = r[1:]
  endif
  return [l, r]
enddef

def Go(...)
  if !a:0
    var &operatorfunc = matchstr(expand('<sfile>'), '[^. ]*$')
    return 'g@'
  elseif a:0 > 1
    var [lnum1, lnum2] = [a:1, a:2]
  else
    var [lnum1, lnum2] = [line("'["), line("']")]
  endif

  var [l, r] = surroundings()
  var uncomment = 2
  var force_uncomment = a:0 > 2 && a:3
  for lnum in range(lnum1,lnum2)
    var line = matchstr(getline(lnum),'\S.*\s\@<!')
    var [l, r] = strip_white_space(l,r,line)
    if len(line) && (stridx(line,l) || line[strlen(line)-strlen(r) : -1] != r)
      var uncomment = 0
    endif
  endfor

  if get(b:, 'commentary_startofline')
    var indent = '^'
  else
    var indent = '^\s*'
  endif

  let lines = []
  for lnum in range(lnum1,lnum2)
    var line = getline(lnum)
    if strlen(r) > 2 && l.r !~# '\\'
      var line = substitute(line,
            \'\M' . substitute(l, '\ze\S\s*$', '\\zs\\d\\*\\ze', '') . '\|' . substitute(r, '\S\zs', '\\zs\\d\\*\\ze', ''),
            \'\=substitute(submatch(0)+1-uncomment,"^0$\\|^-\\d*$","","")','g')
    endif
    if force_uncomment
      if line =~ '^\s*' . l
        var line = substitute(line,'\S.*\s\@<!','\=submatch(0)[strlen(l):-strlen(r)-1]','')
      endif
    elseif uncomment
      var line = substitute(line,'\S.*\s\@<!','\=submatch(0)[strlen(l):-strlen(r)-1]','')
    else
      var line = substitute(line,'^\%('.matchstr(getline(lnum1),indent).'\|\s*\)\zs.*\S\@<=','\=l.submatch(0).r','')
    endif
    call add(lines, line)
  endfor
  call setline(lnum1, lines)
  var modelines = &modelines
  try
    set modelines=0
    silent doautocmd User CommentaryPost
  finally
    var &modelines = modelines
  endtry
  return ''
enddef

def Textobject(inner)
  var [l, r] = surroundings()
  var lnums = [line('.')+1, line('.')-2]
  for [index, dir, bound, line] in [[0, -1, 1, ''], [1, 1, line('$'), '']]
    while lnums[index] != bound && line ==# '' || !(stridx(line,l) || line[strlen(line)-strlen(r) : -1] != r)
      var lnums[index] += dir
      var line = matchstr(getline(lnums[index]+dir),'\S.*\s\@<!')
      let [l, r] = strip_white_space(l,r,line)
    endwhile
  endfor
  while (a:inner || lnums[1] != line('$')) && empty(getline(lnums[0]))
    var lnums[0] += 1
  endwhile
  while a:inner && empty(getline(lnums[1]))
    var lnums[1] -= 1
  endwhile
  if lnums[0] <= lnums[1]
    execute 'normal! 'lnums[0].'GV'.lnums[1].'G'
  endif
enddef

command! -range -bar -bang Commentary call go(<line1>,<line2>,<bang>0)
xnoremap <expr>   <Plug>Commentary     <SID>go()
nnoremap <expr>   <Plug>Commentary     <SID>go()
nnoremap <expr>   <Plug>CommentaryLine <SID>go() . '_'
onoremap <silent> <Plug>Commentary        :<C-U>call <SID>textobject(get(v:, 'operator', '') ==# 'c')<CR>
nnoremap <silent> <Plug>ChangeCommentary c:<C-U>call <SID>textobject(1)<CR>
nmap <silent> <Plug>CommentaryUndo :echoerr "Change your <Plug>CommentaryUndo map to <Plug>Commentary<Plug>Commentary"<CR>

if !hasmapto('<Plug>Commentary') || maparg('gc','n') ==# ''
  xmap gc  <Plug>Commentary
  nmap gc  <Plug>Commentary
  omap gc  <Plug>Commentary
  nmap gcc <Plug>CommentaryLine
  nmap gcu <Plug>Commentary<Plug>Commentary
endif

# vim:set et sw=2:
